import "./Search.css";

const Search = (props) => {
  return (
    <div className="Search_margin">
      <label>Search Username</label>
      <br />
      <input type="text" placeholder="Search"></input>
      {props.getPlayerData?.map((val, key) => {
        return <div>{val.username}</div>;
      })}
    </div>
  );
};
export default Search;
